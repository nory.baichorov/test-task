export interface UserInterface {
    id: number,
    name: string,
    phone: string,
    username: string,
    email: string,
    website: string,

    company: {
        bs: string,
        catchPhrase: string,
        name: string
    },

    address: {
        city: string,
        street: string,
        suite: string,
        zipcode: string,
        geo: {
            lat: string,
            lng: string
        }
    }
}

export interface UserProps {
    users: UserInterface[]
}