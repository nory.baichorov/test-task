import axios from 'axios';

const GET_USERS_URL = 'https://jsonplaceholder.typicode.com/users';

export const getUsers = async () => {
   try {
     return await axios.get(GET_USERS_URL);
   } catch (e) {
     console.log('Error from (function getUsers)', e);
   }
};