import React from 'react';
import cn from 'classnames'

import styles from './Button.module.scss';
import { ButtonProps } from "./Button.props";

const Button = ({ children, type, className, appearance = 'primary', isDisabled = false, ...props }: ButtonProps): JSX.Element => {
    return (
        <button
            className={cn(styles.button, className, {
            [styles.primary]: appearance === 'primary',
            [styles.ghost]: appearance === 'ghost',
            [styles.green]: appearance === 'green',
        })}
            disabled={isDisabled}
            type={type}
            {...props}>
            {children}
        </button>
    );
};

export default Button;