import React from 'react';

import preloader from '../../assets/preloader.svg';

const Preloader = (): JSX.Element => {
    return (
        <img src={preloader} alt='' />
    );
};

export default Preloader;