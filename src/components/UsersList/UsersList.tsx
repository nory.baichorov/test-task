import React, { useEffect, useState } from 'react';

import Card from "./Card/Card";
import Htag from "../Htag/Htag";
import Button from "../Button/Button";
import styles from "./UserList.module.scss";
import { UserProps } from "../../interfaces/UserInterface";

const UsersList = ({ users }: UserProps): JSX.Element => {
    const [newUsers, setNewUsers] = useState(users);
    const [isSortedByCity, setIsSortedByCity] = useState<boolean>(true);
    const [isSortedByCompany, setIsSortedByCompany] = useState<boolean>(true);

    const sortUsersByCity = () => {
        isSortedByCity
            //@ts-ignore
            ? setNewUsers(users.sort((a, b) => a.address.city.localeCompare(b.address.city)))
            //@ts-ignore
            : setNewUsers(users.sort((a, b) => b.address.city.localeCompare(a.address.city)))
    };

    const sortUsersByCompany = () => {
        isSortedByCompany
            //@ts-ignore
            ? setNewUsers(users.sort((a, b) => a.company.name.localeCompare(b.company.name)))
            //@ts-ignore
            : setNewUsers(users.sort((a, b) => b.company.name.localeCompare(a.company.name)))
    };

    useEffect(() => {
        return () => {};
    });

    return (
        <>
            <div className={styles.buttonsWrapper}>
                <Button onClick={() => {
                    sortUsersByCity();
                    setIsSortedByCity(!isSortedByCity);
                }}>
                    По городу {isSortedByCity ? '(A-Z)' : '(Z-A)'}
                </Button>
                <Button onClick={() => {
                    sortUsersByCompany();
                    setIsSortedByCompany(!isSortedByCompany);
                }}>
                    По компании {isSortedByCompany ? '(A-Z)' : '(Z-A)'}
                </Button>
            </div>
            <Htag>Список пользователей</Htag>
            { newUsers.map(user =>  <Card key={user.id} userId={user.id} name={user.name} company={user.company} address={user.address} /> )}
            <div>Найдено {users.length} пользователей</div>
        </>
    );
};

export default UsersList;