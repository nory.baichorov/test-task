import { DetailedHTMLProps, HTMLAttributes } from "react";

export interface CardProps extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  userId: number,
  name: string,
  address: {
    city: string
  },
  company: {
    name: string
  }
}