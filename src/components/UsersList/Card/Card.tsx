import React from 'react';

import styles from './Card.module.scss';
import { CardProps } from "./Card.props";
import CustomLink from "../../CustomLink/CustomLink";

const Card = ({ userId, name, address, company, ...props }: CardProps): JSX.Element => {

    return (
        <div className={styles.cardWrapper}>
            <div className={styles.usersDataWrapper}>
                <span>ФИО: {name}</span>
                <span>Город: {address.city}</span>
                <span>Компания: {company.name}</span>
            </div>
            <div className={styles.linkWrapper}>
                <CustomLink href={`/users/${userId}`}>Подробности</CustomLink>
            </div>
        </div>
    );
};

export default Card;