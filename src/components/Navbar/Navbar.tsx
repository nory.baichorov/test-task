import React from 'react';

import styles from './Navbar.module.scss';
import CustomLink from "../CustomLink/CustomLink";

const Navbar = (sortUsersByCity: any): JSX.Element => {
    return (
        <nav className={styles.navbar}>
            <div className={styles.linkWrapper}>
                <CustomLink href={'/users'}>На страницу пользователей</CustomLink>
            </div>
        </nav>
    );
};

export default Navbar;