import { DetailedHTMLProps, HTMLAttributes, ReactNode } from "react";

export interface CustomLinkProps extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement>{
    children: ReactNode;
    href?: string;
}
