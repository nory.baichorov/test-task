import React from 'react';
import cn from 'classnames';
import { Link } from "react-router-dom";

import styles from './CustomLink.module.scss';
import { CustomLinkProps } from "./CustomLink.props";

const CustomLink = ({ children, className, href, ...props}: CustomLinkProps): JSX.Element => {
    return (
        <div
            className={cn(styles.customLink, className, {})}
            {...props}
        >
            {
                href
                    ? <Link to={`${href}`} className={styles.customLink}>{children}</Link>
                    : <>{children}</>
            }
        </div>
    );
};

export default CustomLink;