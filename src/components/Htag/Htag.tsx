import React from 'react';

import { HtagProps } from "./Htag.props";

const Htag = ({ children }: HtagProps): JSX.Element => {
    return (
        <h4>
            {children}
        </h4>
    );
};

export default Htag;