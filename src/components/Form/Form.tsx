import React, { useEffect, useState } from 'react';
import { useParams } from "react-router-dom";

import Htag from "../Htag/Htag";
import Input from "./Input/Input";
import Button from "../Button/Button";
import styles from './Form.module.scss';
import TextArea from "./TextArea/TextArea";
import { UserProps } from "../../interfaces/UserInterface";

const Form = ({ users }: UserProps): JSX.Element => {
    const [isDisable, setIsDisable] = useState<boolean>(true);
    const { userId } = useParams();

    // Храним значение каждого инпута
    const [name, setName] = useState('');
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [street, setStreet] = useState('');
    const [city, setCity] = useState('');
    const [zipcode, setZipcode] = useState('');
    const [phone, setPhone] = useState('');
    const [website, setWebsite] = useState('');
    const [comment, setComment] = useState('');

    useEffect(() => {
        users.map(user => {
            if (Number(userId) === user.id) {
                setName(user.name);
                setUsername(user.username);
                setEmail(user.email);
                setStreet(user.address.street);
                setCity(user.address.city);
                setZipcode(user.address.zipcode);
                setPhone(user.phone);
                setWebsite(user.website);
            }
            return users;
        });
        return () => {};
    }, [users, userId]);

    const onClickSubmit = () => {
        console.log({ name, username, email, street, city, zipcode, phone, website, comment });
    };

    return (
        <div className={styles.pageWrapper}>
            <div className={styles.upperWrapper}>
                <Htag>Профиль пользователя</Htag>
                <div>
                    <Button
                        appearance={!isDisable ? 'ghost' : 'primary'}
                        disabled={!isDisable}
                        onClick={() => setIsDisable(!isDisable)}
                    >
                        Редактировать
                    </Button>
                </div>
            </div>

            <form className={styles.formWrapper}>
              <Input onChange={e => setName(e.target.value)} value={name} disabled={isDisable} label='Name' />
              <Input onChange={e => setUsername(e.target.value)} value={username} disabled={isDisable} label='Username' />
              <Input onChange={e => setEmail(e.target.value)} type='email' value={email} disabled={isDisable} label='E-mail' />
              <Input onChange={e => setStreet(e.target.value)} value={street} disabled={isDisable} label='Street' />
              <Input onChange={e => setCity(e.target.value)} value={city} disabled={isDisable} label='City' />
              <Input onChange={e => setZipcode(e.target.value)} value={zipcode} disabled={isDisable} label='Zip Code' />
              <Input onChange={e => setPhone(e.target.value)} value={phone} disabled={isDisable} label='Phone' />
              <Input onChange={e => setWebsite(e.target.value)} value={website} disabled={isDisable} label='Website' />
              <TextArea onChange={e => setComment(e.target.value)} value={comment} disabled={isDisable} label='Comment' />

                <div style={{textAlign: 'end'}}>
                  <Button
                      type='button'
                      onClick={onClickSubmit}
                      appearance={isDisable ? 'ghost' : 'green'}
                      disabled={isDisable}
                  >
                      Отправить
                  </Button>
              </div>

            </form>
        </div>
    );
};

export default Form;