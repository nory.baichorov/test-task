import React from 'react';
import cn from 'classnames';

import styles from '../Form.module.scss';
import { TextAreaProps } from "./TextArea.props";

const TextArea = ({ name, className, label, isDisable = false, ...props}: TextAreaProps): JSX.Element => {
    return (
        <div className={styles.fieldsWrapper}>
            <label htmlFor={name} className={styles.label}>{label}</label>
            <textarea
                id={name}
                disabled={isDisable}
                className={cn(styles.textarea, className, {})}
                {...props}
            />
        </div>
    );
};

export default TextArea;