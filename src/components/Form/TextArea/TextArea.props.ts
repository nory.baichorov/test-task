import { DetailedHTMLProps, TextareaHTMLAttributes } from "react";

export interface TextAreaProps extends DetailedHTMLProps<TextareaHTMLAttributes<HTMLTextAreaElement>, HTMLTextAreaElement>{
    name?: string,
    isDisable?: boolean,
    label?: string,
}