import React from 'react';
import cn from 'classnames';

import styles from '../Form.module.scss';
import { InputProps } from "./Input.props";

const Input = ({ name, type, className, value, label, isDisable = false, ...props }: InputProps): JSX.Element => {
    return (
        <div className={styles.fieldsWrapper}>
            <label htmlFor={name} className={styles.label}>{label}</label>
            <input
                id={name}
                value={value}
                type={type}
                disabled={isDisable}
                className={cn(styles.input, {})}
                required
                {...props}
            />
        </div>
    );
};

export default Input;