import { InputHTMLAttributes } from "react";

export interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
    name?: string,
    label?: string,
    isDisable?: boolean,
    value?: string,
    type?: string
}