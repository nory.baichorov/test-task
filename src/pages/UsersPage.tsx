import React from 'react';

import UsersList from "../components/UsersList/UsersList";
import { UserProps } from "../interfaces/UserInterface";

const UsersPage = ({ users }: UserProps): JSX.Element => {
    return (
        <>
            <UsersList users={users} />
        </>
    );
};

export default UsersPage;