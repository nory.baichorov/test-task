import React from 'react';

import { UserProps } from "../interfaces/UserInterface";
import Form from "../components/Form/Form";

const UserPage = ({ users }: UserProps): JSX.Element => {
    return (
        <>
            <Form users={users} />
        </>
    );
};

export default UserPage;