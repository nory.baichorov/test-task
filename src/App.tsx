import React, { useEffect, useState } from 'react';
import { Route, Routes } from "react-router-dom";

import styles from './App.module.scss';
import UserPage from "./pages/UserPage";
import UsersPage from "./pages/UsersPage";
import { getUsers } from "./api/getUsers";
import Navbar from "./components/Navbar/Navbar";
import Preloader from "./components/Preloader/Preloader";

const App = (): JSX.Element => {
    const [users, setUsers] = useState([]);
    const [isLoaded, setIsLoaded] = useState<boolean>(false);

    useEffect(() => {
        setIsLoaded(true);
        getUsers()
            .then(res => {
                setUsers(res?.data);
                setIsLoaded(false);
            })
            .catch(err => console.log(err));
        return () => {};
    }, []);

  return (
    <div className={styles.wrapper}>
      <Navbar />
        {
            isLoaded
                ? <div className={styles.preloaderWrapper}>
                    <Preloader />
                  </div>
                : <div className={styles.contentWrapper}>
                    <Routes>
                        <Route path='/users' element={<UsersPage users={users} />}/>
                        <Route path='/users/:userId' element={<UserPage users={users} />}/>
                    </Routes>
                </div>
        }
    </div>
  );
};

export default App;
